This diary file is written by Samuel Chen E14066436 in the course Professional skills for engineering the third industrial revolution.

# 2020-09-17 #

* Talks about different conflict behavior.
* Learns about GIT and what we use it for in this course.
* See different perspectives of exponetial growth by listening through other group's presentation.
* The teacher made a bold prediction that oil usage has reached its peak and will never be higher than 2019.
* Learns all different perspectives of comparing the past and preseent from TED talk video.

# 2020-09-24 #

* The teacher gave us lots of feedback on our presentation to help us improve on presentation skills.
* Watched some videos talking about fake news and statistics.
* Teach us how to give citation.
* Briefly talk about how to detect fake news.

# 2020-10-08 #

* Did the presentation for the first time, was a little nervous.
* Learned that the Bretton Woods system was already non-existing since 1972.
* Watched a long video about how the economic machine works.
* Learned that credit played such a large role in our economic system.

# 2020-10-15 #

* This week's presentation had lots of problems, lots of the graphs didn't include description on axises.
* Learned more about economics across different countries and especailly Taiwan.
* Learned a little more about how debt works across banks and government.
* Was shocked to realize lots of things I thought were foundation of living are actually imagined realities.

# 2020-10-22 #

* Everyone had some interesting fictional stories, some from other cultures were cool.
* The heart rate variability experiment was interesting too, had a lot of fun.
* The path of knowledge is a new way of thinking for me and i think will be helpful in the future.
* The brain-changing benefits of exercise is nice, and i will continue to exercise frequently.

# 2020-10-29 #

* This week our presentation was suddenly changed, combining groups' presentation, which caused some panic.
* The topic of this week is about depression, the videos were pretty touching.
* I like the video about the San Francisco golden gate brigde, where Kevin Briggs talks about his work life surrounding those attempting suicide.

# 2020-11-05 #

* This week some presentations shared some touching stories, and Val shared her personal experience which I think is very brave of her.
* The teacher gave a important point to help yourself before helping others.
* Finding the meaning of life is something i think is important as well, and i will try to find it.
* The rules we came up are: Please those you care about, and set short-term and long-term goals to try and accomplish.

# 2020-11-12 #

* This week every group did some deep research on different topics, and I find the UBI one interesting.
* Doing further research on the monetary system due to the group project made me understand more about economics and the flaws of it.
* The concept of the Strawman video is pretty new to me, for I have never viewed the legal system from this perspective.
* In the video there was a part that said," When you were born, a company with your name was created through birth certificate." It was shocking to me.

# 2020-11-19 #

* This week's presentation was about laws and legal systems, which i am more unfamiliar with.
* Knowing some differences between country's laws is interesting.
* The videos this week were kind of divergent, didn't really understand the connection between them and the discussion.

# 2020-11-26 #

* We did an interesting experiment on smartphone addiction by collecting all smartphones for three hours of class.
* The video" fall of empires" was a shocking truth to me, as I didn't know history would repeat itself even for a strong country like the US.
* I didn't know that UBI has been experimented so much around the world until the presentation.

# 2020-12-03 #

* I didn't attend class, so I watched the videos," Let the environment guide our development" and" A healthy economy should be designed to thrive, not grow."
* The first video introduced what the nine planetary boudaries are, and current situations of each of them.
* The second video talks about growth of economy, and why it needs to thrive, not grow.
* A healthy balance is more important than constant growth.

# 2020-12-10 #

* This week our presentation added a panel in the end, where every group would have a member represent and be asked questions.
* Then we ran for our action to improve on the planetary boundries, and vote for the best action in the end.
* It was a pretty cool experience, as I normally don't have the chance to be someone who would do such things.

# 2020-12-17 Thu #

* A. Unsuccessful and unproductive
* B. I felt unsuccessful because I got the grades from fluid dynamics and it was pretty bad. I was unproductive because after class i had team practice so i couldn't study for the night.
* C. I will have time to study tomorrow and probably will be more productive.

# 2020-12-18 Fri #

* A. Successful and productive
* B. I feel successful and productive because our group finished our discussion on the group project today.
* C. I will go to Chiayi tomorrow to play a match.

# 2020-12-19 Sat #

* A. Successful and unproductive
* B. I felt successful because we won a match against NCHU and i played two third of the game. But I was tired afterwards so I didn't study at all.
* C. Tomorrow will be a day off so I hope I will study.

# 2020-12-20 Sun #

* A. Unsuccessful and unproductive
* B. We had a meeting about the game yesterday and spent lots of time, other time was spent napping and chatting with my teammates.
* C. Tommorrow there will be an early game against a tough opponent. I don't know if i will have energy to study afterwards.

# 2020-12-21 Mon #

* A. Successful and unproductive
* B. We tied against STU, and i played alomost the whole match which was successful but tiring. I went back to Tainan but didn't study too much.
* C. Tommorrow will be a day off, hope I have the time to study.

# 2020-12-22 Tue #

* A. Unsuccessful and productive
* B. I was tired from yesterday so i had a slow day, still managed to study some machine component design.
* C. Tommorrow I will go to Chiayi for an important game against our rival NTU.

# 2020-12-23 Wed #

* A. Successful and unproductive
* B. We won 1-0 against NTU, which made me feel super successful, I played full match so I'm tired after coming back, which caused me to be unproductive.
* C. I will wake up early tommorrow morning to finish my homework.

# Five rules for success #

* Be kind
* Laugh more
* Remember to come back from your lows
* Choose a goal and go for it
* Follow your own rule

# 2020-12-24 Thu #

* A. Successful and unproductive
* B. I was unproductive for watching videos of matches we played so I didn't study, but I enjoy my performance on the field so I felt successful.
* C. I will be more productive tommorrow.

# 2020-12-25 Fri #

* A. unsuccessful and productive
* B. We finished our group project so I feel productive, I didn't have a date on Christmas so I feel unsuccessful.
* C. I will go to a Christmas party tommorrow.

# 2020-12-26 Sat #

* A. Successful and unproductive
* B. We had fun on the party which made me feel successful, but I didn't study so it's unproductive.
* C. I will have a group meeting tommorrow and plan on studying for a test on Monday.

# 2020-12-27 Sun #

* A. Unsuccessful and productive
* B. I finished another group project in another class which made me feel productive, I felt unsuccessful because I wasn't focused on my study.
* C. I will try harder to focus.

# 2020-12-28 Mon #

* A. Successful and unproductive
* B. I did well on the test so i feel successful, but I still failed to focus well because I started watching youtube.
* C. I will try to not lose focus on other things.

# 2020-12-29 Tue #

* A. Successful and productive
* B. I had a late Christmas party and I had fun, I got a nice present and my preseent wasn't voted to be bad, which made me feel successful. And because I have a party so I was more focused on studying on the afternoon.
* C. Hope I can keep up with the productivity.

# 2020-12-30 Wed #

* A. Unsuccessful and unproductive
* B. It was a cold day so I kept going back to my warm blanket, which made me unproductive and unsuccessful.
* C. I will try to tie myself in front of my desk instead of my bed.

# 2020-12-31 #

* The concept of choosing a boss over choosing a job is cool, though I think it is not very common in Taiwan.
* The video from buzzfeed was interesting to learn about how to go viral.
* It was also interesting to know why the professor wanted to teach this class.