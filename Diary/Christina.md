This diary file is written by Christina F94085321 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* Introduced to GIT Repository (a bit tricky to use, but love the neat and simple layout), wondering why GIT Repository instead of Google Drive.
* Glasl conflict escalation model is interesting.
* Give a presentation and received a lot of feedbacks from Prof (realised the importance of formatting details in making the slides look professional).
* I think exponential might not happen forever, eg. electronic components have physical size limitation, they can't be continually shrunk forever.
* Got new insights about the 5 major pillars of industrial revolution 4.0.
* The Covid-19 pandemic has put us into a setback in reaching SDGs.


# 2021-09-30 #

* Lecture was quite boring
* Why is there so many citation styles?
* How impactful is the presence of fact checking organizations in the midst of information overload? (i.e. how to fact-check a sea of information given the fact that this kind of organizations is not that ubiquitous yet)
* Will there always be more than one news for every event (esp. small events) to crosscheck?
* Talk by Mona Chalabi is insightful: 
	* demonstrated how inaccurate is polling
	* seems like standard deviation (or sth similar to it) should be added to each number so that readers know what to expect on the variability
	* just wondering if data from governmental institution are really more credible (eg. comparing to independent survey institutions)
* Spotting fake news on Facebook: did not spot one on my timeline 
	* news were mainly from verified pages i subscribed
	* suggestions were mostly from "pretty reliable" sources, click-bait content being the worst
.
