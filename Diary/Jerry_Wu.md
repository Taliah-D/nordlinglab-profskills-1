# 2019-09-19

* i think that people indeed think too much about how the world is going to change except global warming and pollutions.
* If the media doesn't describe those changes as dramatically as they could, I think people just tend to ignore it, like me.
* If a product makes one's life a lot easier, i guess it's not very surprising that its quantity doubles every year.
* The price will drop once there are more and more same product in the market, but it takes time and it might not happen if there's only one big company owning the market.

# 2019-09-26

* It's mostly been listening the reports from others, it is good to see people explaining their ideas though I got a bit bored later on.
* I think putting references right next to the charts is really good because I really hate that when reading textbooks I have to check over and over if I read about something idk.

# 2019-10-03

* Banks are everywhere and seemed very common, but I still don't know the concept of it until I watch the vids.
* Credit is even more important than money since its quantity is much higher than money.
* Borrowing money is easy, but using the money to create more it's the problem that matters.
* I think being in debt is very similar to studying. If you play first you still need to study later, so we need to control the balance in case the recessions to be too subtle.

# 2019-10-17

* Extremists make you think something is good while it's not or even horrible
* fascism is different than nationalism
* The way to convince people to believe or care about something is to show that it is happening around us. 
* Never underestimate human's stupidity

# 2019-10-24

* I think the idea of supergroup it's nice and i feel more involved.
* To live healthy we need to excercise.
* excercise 3 times a week make our brain stronger and prevent deceases.
* The experiment is fun to watch but definitely not fun to be the participants.

# 2019-10-31

* To the prople who suffer from depression, the problem is real.
* People who attempt suicide really need to step back and think again.
* We can help by listening to them, don't just told them to walk it off.

# 2019-11-07

* Post hoc construction proofs that anyone can be easily manipulated.
* Self-knowledge is actually self-interpretation.
* Happiness isn't the only important thing in our lives.
* There are 4 things that can make our lives more meaningful, belonging, purpose, story, transcendence.

# 2019-11-14
* It's important to state your right peacefully and without harming others.
* if someone break into other people's house or touching them without permission is assault.
* no creation by people can be above people.
* Police can't check anyone's detail for no valid reasons.

# 2019-11-21
* Technology supposed to bring people closer, but sometimes it's quite the opposite.
* To connect with people, it would be better to talk face to face.
* Smartphones are just as addictive as gamble and alcohol.

# 2019-11-28
* It's important that currency maintains its value.
* The value of a currency is decided by its quantity.
* In history, inflation happened again and again and it causes empires to end.

# 2019-12-05
* After listening to the presentations, I was surprised that so many things can happen in a week.
* The Earth is at danger and we need to take actions to protect it as soon as possible.
* Planetary boundaries were defined by tipping points, which means the results became 
  irreversible and small change may lead to huge impact.
* Limits stimulate our creativity.

# 2019-12-12
* Today's lecture is kinda fun and I think the idea of proposing an action that everyone can do is really helpful.
* All of the boundaries are important, and we should think about all the other creatures instead of just human beings.
* It's good that we talked about the subjects through rational debates.

# 20191219 Thu
* A. unsuccessful and unproductive
* B. I felt unsuccessful and unproductive because I tried to take a break by playing video game but I was still a bit stressed.
* C. Play after finishes my homeworks.
# 20191220 Fri
* A. unsuccessful and productive
* B. I felt unsuccessful because I got sick and I couldn't focus on the course in the morning. I felt productive because I managed to finished 
*    the final project of mechanical drawing course with my friends.
* C. Avoid staying up late by planning my schedule ahead.
# 20191221 Sat
* A. unsuccessful and produuctive
* B. I felt unsuccessful because I spent too much time waiting while I was in the clinic and though I came up another design of the steering rack, 
*    the progress overall was stuck. I felt productive because  I thought the medicine worked.
* C. Leave it for a while and come back later if I'm out of ideas.
# 20191222 Sun
* A. successful and unproductive
* B. I felt unproductive because my beam design for the final project of Machnics of Materials was still worse than my friend's after spending a 
*    lot of time improving it. I felt successful because we hangout for lunch after we finished the project and I felt less stressed.
* c. Do outdoor activites when I feel stressed out.
# 20191223 Mon
* A. successful and productive
* B. I felt successful because I helped my friend to ship ship the materials he made. I felt productive because during my group's meeting, we discussed about the rack design and we came up with some good ideas.
* C. I will modify the last design and start to run some simulations.
# 20191224 Tue
* A. successful and productive
* B. I felt successful because I could finally be focused on the lectures and not dosing off or daydeaming. I felt productive because I was
*    steadily making progress on my bench press.
* C. Find something I like to do regularly that can ease up my stress.
# 20191225 Wed
* A. successful and productive
* B. I felt successful because I checked my body composition and it had improved. I felt productive because I started to study to catch up the 
*    courses' progress.
* C. Go to bed before 12.
# 5 rules
* Keep myself in a good mood.
* Do my homework first.
* Be aware of course announcements and write it down.
* Attend the courses on time.
* Sleep 7-8 hours a day.

# 2019-12-26
* I feel a little bad because I lost my focus in the last 40 minutes.
* To determine whether a source is trustworhy or not is hard because even if the website shows some stats to justify the information, We still don't know
  those numbers is reasonable or not.