## 2019-09-26 

1. First time go to the class 
2. Learn more about the main topic of this class 
3. Main topic talk more about the fake news ore hoax 
4. For me personally, fake news is kind of dangerous problem. Because in my country, there are still alot of un-open-minded people who can easily believe the hoax then turn into provocation.

## 2019-10-03 

1. Talking more about the economics and how the bank works. 
2. For me personally it's a new things, because i never learn the economics. 
3. After watching the video, i'm realize that money is abstract nowadays.
4. I'm also get alot of information about the spreading of "real" money and "digital" money.

## 2019-10-17 

1. For today's topic, we learn more about the fasism things. 
2. For me personally, this is new thing i can learn about the nationalism, eventhough it has alot of different with my country. 
3. But in one of the video, i have something curious about the statement that said the facism is different with the nationalism, because for my point of view, facism is one of the way for a person or people      that believe in it to show their nationalism, despite the pros and the cons.

## 2019-10-24

1. For today's topics, we learn more about the health issues, especially the brain health.
2. In my personal opinion, the brain health is very important because the brain itself is the "father" of our body. So when the brain health is good, i think the rest of the body will also have a good condition.
3. In the video, i like how the speaker let us do some exercise. Because actually that's a simple movement that will have some good impacts to our body. So it's like prove to people that healthy life doesn't require any hard level of exercise, just do some correct simple movements, than it will good for your body.

## 2019-10-31

1. For today's topics, we talking more about the depression.
2. I have personal experience about friend with depression who tried to commit suicide. For me as friend, it's one of the worst experience that i have. And if i have time to go back to the past, i'll try to help him to escape from his depression.
3. According to the video, i'm totally agree with the speaker opinion. Because i have real experience about it and if all of us know how to deal with it, i think we can save many people lifes just with simple but correct ways.

## 2019-11-07

1. For today's topics, we talking more about pursuing happiness.
2. In my opinion, the "happiness" itself isn't one thing that we should find or create, but something that we should enjoy.
3. Because based on the video, the problem is when the people busy to find the happiness for themselves, they become stress because of the obsession of the "happiness" itself.

## 2019-11-14

1. For today's topic we talking more about law.
2. In my opinion, i have no interest with law, because in my country the law itself didn't have any proportion.
3. Because the law sometimes just sharp to the low people and invisible to the high level people. So law just kind of "template" games in the end for me.

## 2019-11-21

1. For today's topic we talking more about technology that "distract" us with the real life.
2. In one of the video, i'm agree with the speaker that sometimes the cellphone or the social media distract us with reality that we currently facing. For example in the meeting, we supposed to listen and try to share our idea, but we focused with the cellphone rather than the people we currently facing.
3. In simple case, in the dining room, when it's the time to make some quality time with family or friends, we are busy to connect with other people in other place.
4. In my opinion, the technology simply want us to make the people far from us, became near with us. But in our current reality, it just resulting with make the people near us became far.

## 2019-11-28

1. For today's lecture we talking more about how to make sens of events.
2. In on of the video, the speaker talk about the difference between opinion on facts. This is interesting for me because sometimes, a lot of people still can't categorize it carefully. Because in most case they just assume that some big opinion as a fact which is wrong.
3. Then in today's class we also learn about manage the small factor which is very important. Because in some of the cases, we over anticipating with the big problem, but actually the small problem kills slowly.
4. In other word, the small problem looks like a snowball which is look like a small thing in the beginning, but can grow bigger and bigger if we never destroy it.
5. But the most important thing is , most people only see what they want to see, do they want to do. So sometimes, it depends on each personal to solve their own problem.

## 2019-12-05

1. For today's lecture we talking more about the planetary boundaries.
2. My group will particularly talk about the ocean acidification from the several kinds of planetary boundaries.
3. I think, for today's lecture, the class focused on the news presentation, and for me personally it was interesting. Because it compare some news data from eseveral country, so at the end we can collect a lot of comparison between the country that presented.
4. We also have some discussion about the problem from the presentation before. It was interesting because we can share many ideas between one and another group.
5. Then we also watch some video that talk about gender equality. For me, it's common problem in most of the asia nations, also in my country. And the main problem is all about the paradigm that we already heard even from our parents when we still a child. So the solution is try to remove the paradigm in society, and the gender equality problem will decrease as soon as possible.

## 2019-12-12

1. For today's lecture we spent most of the time by debating.
2. The main topics of this debate session was the Planetary Boundaries which is have alot of relations with environmental issues.
3. For me personally, the most important planetary boundary is climate change, because it can cause another planetary boundary issues such as ocean acidification.
4. The most interesting part of today's lecture was, in the end of the class, the teacher give us a chance to vote about the planetary boundaries, because it's like some "cooling down" part after the debate session.

## 2019-12-19

1. For today's lecture we spent most of the time by presentation of last week task.
2. I think it's interesting because, most of us can present a lot of answer.
3. And on the next part of the lecture, the class was divided by three groups, the environmentalist, the workers, and the capitalist.
4. For me as the workers, the debate should be just between the two groups, environmentalist and the capitalist.
5. After debate session ended, we continue with watching video.
6. For me the video is interesting, because it teach us about productive.

## 2019-12-26

*	2019-12-26
	a. successfull and productive
	b. just finished homework correctly
	c. try to wake up early
*	2019-12-27
	a. unsuccessful and unproductive
	b. have to prepare exam, resulting didn't wake up for morning class
	c. rest time priority
*	2019-12-28
	a. unsuccessful and productive
	b. got all-day long shift in my part-time work, too tired to study after back from work
	c. try to manage some times
*	2019-12-29
	a. successful and productive
	b. go to the church, back to work
	c. excercise before doing a task
*	2019-12-30
	a. successful and productive
	b. do some therapy for my foot injury
	c. focus more on health
*	2019-12-31
	a. successful and productive
	b. feeling energetic all day, celebrating the christmas eve
	c. 
*	2019-01-01
	a. unsuccessful and productive
	b. feeling lazy, skip class, and use the time to do homework and take a nap
	c.
	
## 2020-01-02

Today's is the last lecture of the semester. From this course i have learn alot of new things. And even that this course is about the Industrial Revolution, for me personally i have learn alot of things outside this course main idea. I also want to do some review about this course. I think the idea of making diary every weeks is a good things. That's kind of the trick so we always learn and pay much attention to the course lecture in every minute.
Also made us trying to do some review about the knowledge that we received in every lecture, and i think it is a new methods for me to made myself better in learning in class. Also in every lecture we must do some presentation based on the task that the teacher already given to us in every week before. Sometimes the task is very difficult and sometimes also very easy. The problem of making presentation in every week is about the teammate. Because every three weeks we must change out teammate and every teammate have their own characteristic that we must understand.
And in most of the cases also there are some of the group members that very like to presentate, and also there are some of the group members that not like to presentate because of their english skills. I think sometimes it pretty unfair to make presentation score based only on the presenter. Because in most of the cases another group members also have many contribution in order to make the presentation content. But unfotunately, only the presenter will received the presentation score. Maybe in the next course, this kind of grading problem will have another fair adjustment.
Something that i'm rarely found in another class that in this class we also have to heard some video from TED. In my third year in NCKU, i have only have another one class that also use TED as the reference. Because according my experience with this class, i think the TED video also have alot of contribution in the idea gathering for the class itself. Because sometimes, the teacher also make some discussion times. There are alot of types of the discussion, but the one that i like the most is the debate session. Because in the debate session we can know more about another person point of view and that is interesting.
Also there is some new part in this class that i have never found in another class. In this class we also have some polling session. It's like the open majority polling that if we agree with something, we just stand up then another people will know about our choices. I think it is very interesting because in another type of polling, we just know about the result of the polling itself, but never know who with which idea. And for the last final project idea, we also used the majority polling to choose our final project idea. So there are alot of transparency in this class about sharing the ideas.
But sometimes i also have some difficult part in this class too. Sometimes i feel like making diary in every week also exhausting. Because in most of the cases we write the diary one day before the next class. And because of that long time span, sometimes we also forget about the last class lecture, and we need to watch the video again. And for the diary presentation, in the beginning of the semester, there is alot of chance for the people to sharing the diary in front of the class. But, after the 9th week of the semester, i think it's pretty rare for us to have a chance to sharing about the diary in front of the class.
In the week 15th or 16th, the teacher also want us to make some special diary that we need to write it down in every week. The main idea is to write down about the days inside that week experience about ourself. Is it succesfull or failed, productive or not, and what kind of things that we already do in that day. I think it's interesting because we can do some reflection about ourself in that weeks. Also made us to do some evaluation to make another days better than before. And in the lecture of the class also talking more about the process of ourself. And in some chance, the teacher also need our feedback to write down the class survey about the change of our life because of attending this class.
In many lecture, we also talking about the technology and global conditions in every aspect. I think it's interesting because in one of the video that i remember. It talks about the technology that made us distract with the real life. And it's very ironic. Because if we can't change ourself and just go with the flow with the technology, then in the future we just become anti-social, and just depends of the technology to live our life. We also learn about the planetary boundaries of our planets. It's kind of new information that i got from this class. We also need to do some presentation based on every planetary boundaries available.
And after all of the presentation, the teacher made some survey about which of the planetary boundaries conditions that affect the most. And in this last diary, i want to thank the teacher that made us change from the one who don't know anything about this course become the one who know alot of knowledge because of this class. And for me personally, this class is very interesting, despite of the alot of diary that we must write in every weeks, presentation that we must do in every weeks, even the deep discussion that we must attend, this class is incredible because it give me alot of fun and alot of new knowledge, friends, and information.
I hope that in another chance, i can have this kind of class, because it really changes me in many aspects of myself. Thank you
